using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackHitBox : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(GameObject collision)
    {
        var weapon = GetComponent<AttackController>();
        var player = GetComponent<Player>();
        player.ApplyDamage(collision,weapon.currentWeapon.Damage);
    }
}
