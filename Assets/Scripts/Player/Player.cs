using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour, IDamageable
{
    public static event Action<WeaponData> OnChangedWeapon;
    public UnityEvent<int> onHpChanged;

    private const int MaxHp = 5;

    [Header("Movement")]
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Transform body;
    [SerializeField] private float moveSpeed = 5f;
    [SerializeField] private float jumpForce = 7f;
    [SerializeField] private LayerMask groundLayer;

    [Header("Health")] 
    [SerializeField] private int currentHp = MaxHp;

    [SerializeField] private Transform spawnPoint;
    private Vector2 moveInput;
    private bool isGrounded;
    
    public void ChangeWeapon(WeaponData _newWeapon)
    {
        var weapon = GetComponent<AttackController>();
        weapon.currentWeapon = _newWeapon;
        OnChangedWeapon?.Invoke(weapon.currentWeapon);
    }

    private void Start()
    {
        onHpChanged?.Invoke(currentHp);
    }
    
    #region HP

    public void Heal(int _value)
    {
        currentHp += _value;
        onHpChanged?.Invoke(currentHp);
    }
    
    public void DecreaseHp(int _value)
    {
        currentHp -= _value;
        
        if (currentHp <= 0)
        {
            Death();
        }
        onHpChanged?.Invoke(currentHp);
    }

    private void Death()
    {
        currentHp = MaxHp;
        Respawn();
    }

    private void Respawn()
    {
        rb.velocity = Vector2.zero;
        transform.position = spawnPoint.position;
    }
    
    #endregion
    
    #region Movement

    public void OnMove(InputAction.CallbackContext _context)
    {
        moveInput = _context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!isGrounded) return;
        
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        isGrounded = false;
    }
    
    private readonly float checkGroundRayLenght = 0.6f;
    
    private void FixedUpdate()
    {
        //UpdateMovement
        rb.velocity = new Vector2(moveInput.x * moveSpeed, rb.velocity.y);
        
        // Flip the player sprite when changing direction
        if (moveInput.x != 0)
        {
            var point = GetComponent<AttackController>();
            body.localScale = new Vector3(Mathf.Sign(moveInput.x), 1f, 1f);
            float _rotate = Mathf.Sign(moveInput.x) > 0 ? 0 : 180f;
            point.firePoint.rotation = Quaternion.Euler(0, 0, _rotate);
        }
        
        //CheckGround
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, checkGroundRayLenght, groundLayer);
        
        isGrounded = _hit.collider != null;
    }
    
    private void UpdateMovement()
    {
        
    }

    private void CheckGround()
    {
        
    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }

    #endregion


    public void ApplyDamage(GameObject _source, int _damage)
    {
        DecreaseHp(_damage);
    }
}
